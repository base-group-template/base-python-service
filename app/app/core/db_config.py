from os import environ

import gridfs
from pymongo import MongoClient

client = MongoClient(environ.get('MONGO_CONNECTION_STR'), connect=False)
db = client[environ.get('MONGO_DB_NAME')]
fs = gridfs.GridFS(db)
