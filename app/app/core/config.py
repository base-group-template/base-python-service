from os import environ

from app.core.utils import load_environment_variables

load_environment_variables()


class AppConfig(object):
    JWT_OIDC_WELL_KNOWN_CONFIG = environ.get('JWT_OIDC_WELL_KNOWN_CONFIG')
    JWT_OIDC_AUDIENCE = environ.get('JWT_OIDC_AUDIENCE')
    JWT_OIDC_CLIENT_SECRET = environ.get('JWT_OIDC_CLIENT_SECRET')
    JWT_OIDC_ALGORITHMS = environ.get('JWT_OIDC_ALGORITHMS')
