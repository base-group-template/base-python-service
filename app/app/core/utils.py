from healthcheck import HealthCheck, EnvironmentDump
from dotenv import load_dotenv, find_dotenv
from os import path, environ
import pathlib
import logging

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)

API_ENV = environ.get('API_ENV')


def get_abs_path(): return str(pathlib.Path(__file__).parent.absolute())


def get_env_filename():
    environments_map = {
        'local': '.env.local',
        'feature': '.env.dev',
        'dev': '.env.dev',
        'qa': '.env.qa',
        'test': '.env.test',
        'production': '.env.production'
    }

    if API_ENV in environments_map:
        return environments_map[API_ENV]
    else:
        local = environments_map['local']
        LOGGER.warning('The environment API_ENV={} can not be loaded. '
                       'The "local" environment will be loaded as default.'.format(API_ENV))

        return local


def build_env_file_path(abs_path, env_filename):
    return abs_path + path.sep + env_filename


def load_environment_variables():
    abs_path = get_abs_path()
    env_filename = get_env_filename()
    env_file_path = build_env_file_path(abs_path, env_filename)
    env_file = find_dotenv(env_file_path, raise_error_if_not_found=True)
    if env_file:
        load_dotenv(env_file)
        LOGGER.info('Loading "{}" environment configuration...'.format(env_filename))


def get_roles(claims):
    if 'realm_access' in claims:
        return claims['realm_access']['roles']

    return []


def config_app(app, config):
    app.config.from_object(config)
    app.config['JWT_ROLE_CALLBACK'] = get_roles
    return app


def environment_check():
    if API_ENV:
        return True, "Loaded environment: " + API_ENV
    else:
        return True, "Failed to load the environment: " + API_ENV


def config_health():
    health = HealthCheck()
    health.add_check(environment_check)

    return health


def config_env_dump():
    env_dump = EnvironmentDump()

    def custom_section(): return {"maintainer": "blah", "git_repo": "url"}

    env_dump.add_section("application", custom_section)

    return env_dump
