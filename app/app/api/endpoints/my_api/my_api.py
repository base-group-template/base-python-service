from typing import List

from flask import jsonify
from flask_restx import Resource, Namespace

import app.api.utils.errors as error
from app.api.endpoints.my_api.domain.user_dto import UserDto
from app.api.endpoints.my_api.service.my_service import MyService
from app.api.auth.roles import Roles
from main import API, auth

tests = Namespace('tests', description='Tests related operations.')
API.add_namespace(tests)

test_service = MyService()


@tests.route("/")
class Test(Resource):

    @staticmethod
    @auth.requires_auth
    def get() -> List[UserDto]:
        tests.logger.info('testing the logger')
        return jsonify(test_service.get_users())


@tests.route("/roles/")
class MyRoles(Resource):

    @staticmethod
    @auth.requires_auth
    def get() -> str:
        if auth.validate_roles([Roles.OFFLINE_ACCESS.value]):
            return jsonify(message=test_service.secure_with_roles())

        error.unauthorized_access()
