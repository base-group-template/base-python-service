from typing import List

from app.api.endpoints.my_api.entity.user import User


def find_all() -> List[User]:
    users = [
        User("Alice Example", "alice@example.com"),
        User("Bob Example", "bob@example.com")
    ]

    return users
