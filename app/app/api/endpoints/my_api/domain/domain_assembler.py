from app.api.endpoints.my_api.entity.user import User
from app.api.endpoints.my_api.domain.user_dto import UserDto


class DomainAssembler:

    @staticmethod
    def to_domain(user: User) -> UserDto:
        return UserDto(user['name'], user['email'])
