from typing import List

from app.api.endpoints.my_api.controller.my_controller import MyController
from app.api.endpoints.my_api.domain.user_dto import UserDto

test_controller = MyController()


class MyService:

    @staticmethod
    def get_users() -> List[UserDto]: return test_controller.get_users()

    @staticmethod
    def secure_with_roles() -> str: return test_controller.secure_with_roles()
