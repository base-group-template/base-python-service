from typing import List

import app.api.endpoints.my_api.repository.repository as user_repository
import app.api.utils.utils as utils
from app.api.endpoints.my_api.domain.domain_assembler import DomainAssembler
from app.api.endpoints.my_api.domain.user_dto import UserDto
from app.api.endpoints.my_api.entity.user import User

da = DomainAssembler()


class MyController:

    @staticmethod
    def get_users() -> List[UserDto]:
        users: List[User] = user_repository.find_all()
        result: List[UserDto] = []

        for user in users:
            result.append(da.to_domain(user))

        return result

    @staticmethod
    def secure_with_roles() -> str:
        utils.test()

        return "This is a secured endpoint, where roles were examined in the body of the procedure! " \
               "You provided a valid JWT token"
