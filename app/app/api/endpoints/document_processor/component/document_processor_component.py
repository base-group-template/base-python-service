import io
from typing import Any

import numpy as np
import pytesseract
from cv2 import cv2
from gridfs import GridOut
from slate3k import PDF

from app.api.endpoints.document_processor.domain.assembler import Assembler
from app.api.endpoints.document_processor.domain.file_data_dto import FileDataDto
from app.api.endpoints.document_processor.domain.processed_document_dto import ProcessedDocumentDto
from app.api.endpoints.document_processor.errors.unsupported_media_type_exception import UnsupportedMediaTypeError
from app.api.endpoints.document_processor.repository.processed_document_repository import ProcessedDocumentRepository
from app.api.utils.utils import contains_ignore_case

da = Assembler()


class DocumentProcessorComponent:

    @staticmethod
    def process_receipt(file_data: FileDataDto) -> ProcessedDocumentDto:
        file = ProcessedDocumentRepository.find_file(file_data["id"])

        extracted_text = DocumentProcessorComponent.__extract_text(file)

        file.close()

        transformed_data = DocumentProcessorComponent.__transform_text(extracted_text)

        document_dto = ProcessedDocumentRepository.save_processed_document(
            da.to_processed_document_entity(extracted_text, transformed_data)
        )

        return da.to_processed_document_domain(document_dto)

    @classmethod
    def __extract_text(cls, file: GridOut) -> str:
        content_type = file.metadata["mediaType"]
        if contains_ignore_case(content_type, 'pdf'):
            return DocumentProcessorComponent.__extract_pdf_text(file)
        elif contains_ignore_case(content_type, 'image'):
            return DocumentProcessorComponent.__extract_image_text(file)
        else:
            raise UnsupportedMediaTypeError("The content-type '{}' is not supported.".format(content_type))

    @classmethod
    def __extract_image_text(cls, file: GridOut) -> str:
        np_array = da.to_np_array(file)
        img = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        gray, img_bin = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        gray = cv2.bitwise_not(img_bin)
        kernel = np.ones((2, 1), np.uint8)
        img = cv2.erode(gray, kernel, iterations=1)
        img = cv2.dilate(img, kernel, iterations=1)

        return pytesseract.image_to_string(img, lang='deu')

    @classmethod
    def __extract_pdf_text(cls, file: GridOut):
        image_data = file.read()
        open_pdf_file = io.BytesIO(image_data)
        pdf = PDF(open_pdf_file)
        open_pdf_file.close()

        return pdf.text()

    @classmethod
    def __transform_text(cls, extracted_text) -> Any:
        return "TO DO"
