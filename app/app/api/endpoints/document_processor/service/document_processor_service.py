from app.api.endpoints.document_processor.component.document_processor_component import DocumentProcessorComponent
from app.api.endpoints.document_processor.domain.file_data_dto import FileDataDto
from app.api.endpoints.document_processor.domain.processed_document_dto import ProcessedDocumentDto

receipt_component = DocumentProcessorComponent()


class DocumentProcessorService:

    @staticmethod
    def process_receipt(file_data: FileDataDto) -> ProcessedDocumentDto:
        return receipt_component.process_receipt(file_data)
