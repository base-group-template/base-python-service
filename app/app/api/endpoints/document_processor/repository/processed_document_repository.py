from bson import ObjectId
from gridfs import GridOut

from app.api.endpoints.document_processor.entity.processed_document import ProcessedDocument
from app.core.db_config import db, fs

collection = db['processedDocument']


class ProcessedDocumentRepository:

    @staticmethod
    def find_processed_document(id: str) -> ProcessedDocument:
        return collection.find_one(ObjectId(id))

    @staticmethod
    def save_processed_document(processed_document: ProcessedDocument) -> ProcessedDocument:
        result = collection.insert_one(processed_document)
        return collection.find_one(ObjectId(result.inserted_id))

    @staticmethod
    def find_file(id: ObjectId) -> GridOut:
        return fs.get(id)
