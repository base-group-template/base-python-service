from typing import Any


class ProcessedDocument(dict):
    def __init__(self, extracted_text: str, data: Any):
        dict.__init__(self, extracted_text=extracted_text, data=data)
