import json

from bson import json_util
from flask import jsonify
from flask_restx import Resource, Namespace, reqparse, fields

from app.api.endpoints.document_processor.domain.assembler import Assembler
from app.api.endpoints.document_processor.domain.processed_document_dto import ProcessedDocumentDto
from app.api.endpoints.document_processor.domain.request.file_data_dto_parser import file_data_parser, document_parser_test
from app.api.endpoints.document_processor.errors.unsupported_media_type_exception import UnsupportedMediaTypeError
from app.api.endpoints.document_processor.service.document_processor_service import DocumentProcessorService
from app.api.utils import errors
from main import API, auth

document_processor = Namespace('document-processor', description='Document processing.')
API.add_namespace(document_processor)

receipt_service = DocumentProcessorService()

da = Assembler()

processed_file_model = document_processor.model('ProcessedDocumentDto', {
    'id': fields.String
})

file_data_parser = file_data_parser(reqparse)
document_parser_test = document_parser_test(reqparse)


@document_processor.route("/process-document/")
class DocumentProcessor(Resource):

    @staticmethod
    @auth.requires_auth
    @document_processor.expect(document_parser_test)
    @document_processor.response(200, 'Success', processed_file_model)
    def post() -> ProcessedDocumentDto:
        try:
            args = file_data_parser.parse_args()
            doc = da.to_document_domain(args)

            return jsonify(receipt_service.process_receipt(doc))
        except UnsupportedMediaTypeError:
            errors.unsupported_media_type()
