from flask_restx.reqparse import RequestParser


def file_data_parser(reqparse) -> RequestParser:
    parser = reqparse.RequestParser()
    parser.add_argument('id', dest='id', location='form', required=True, help='Document name')

    return parser


def document_parser_test(reqparse) -> RequestParser:
    parser = reqparse.RequestParser()
    parser.add_argument('fileId', dest='fileId', location='form', required=True, help='File ID')

    return parser
