from typing import Any

import numpy as np
from bson import ObjectId
from flask_restx.reqparse import ParseResult
from gridfs import GridOut

from app.api.endpoints.document_processor.domain.file_data_dto import FileDataDto
from app.api.endpoints.document_processor.domain.processed_document_dto import ProcessedDocumentDto
from app.api.endpoints.document_processor.entity.processed_document import ProcessedDocument


class Assembler:

    @staticmethod
    def to_document_domain(document_args: ParseResult) -> FileDataDto:
        return FileDataDto(
            ObjectId(document_args.id)
        )

    @staticmethod
    def to_processed_document_entity(extracted_text: str, transformed_data: Any) -> ProcessedDocument:
        return ProcessedDocument(extracted_text, transformed_data)

    @staticmethod
    def to_processed_document_domain(processed_document: dict) -> ProcessedDocumentDto:
        return ProcessedDocumentDto(
            str(processed_document['_id'])
        )

    @staticmethod
    def to_np_array(file: GridOut) -> np.array:
        return np.frombuffer(file.read(), np.uint8)
