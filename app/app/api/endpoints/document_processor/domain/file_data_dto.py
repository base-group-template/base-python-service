from bson import ObjectId


class FileDataDto(dict):
    def __init__(self, id: ObjectId):
        dict.__init__(self, id=id)
