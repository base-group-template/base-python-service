import re
from typing import Match


def test() -> str:
    return "utils my_api"


def contains_ignore_case(main: str, search: str) -> Match:
    return re.search(search, main, re.IGNORECASE)
