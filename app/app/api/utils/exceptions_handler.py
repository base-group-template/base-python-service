from flask_jwt_oidc import AuthError

from main import API


@API.errorhandler(AuthError)
def handle_auth_error_exception(e):
    return {'message': e.error}, e.status_code
