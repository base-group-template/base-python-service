from flask_restx import abort


def unauthorized_access():
    abort(
        code=403,
        description="The user doesn't have access to this resource."
    )


def unsupported_media_type():
    abort(
        code=415,
        description="The file format is not supported. Only images and PDFs."
    )
