from app.api.utils import exceptions_handler

from app.api.endpoints.my_api import my_api
from app.api.endpoints.document_processor import document_processor_api
