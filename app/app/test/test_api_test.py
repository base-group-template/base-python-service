import datetime

import jwt
# keep this import! from app.test import client
from app.test import client
from Crypto.PublicKey import RSA


class TestApiTest:
    def test_one(self):
        x = "this"
        assert "h" in x

    def test_root(self, client):
        response = client.get("/")
        data = response.json
        assert "hostname" in data

    def test_auth(self, client):
        response = client.get("/tests/")
        data = response.json
        assert data == \
               {'message': {'code': 'authorization_header_missing', 'description': 'Authorization header is expected'}}


"""
    def test_auth2(self, client):
        # token = self.__token__()
        token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJoQ1JkX3hKTmJtdFR2anpNT0IwMndnbXR3ei16MXg1ZVQzU29CaGt4SFU4In0.eyJleHAiOjE2Mzk4MjUwMzUsImlhdCI6MTYzOTgyMTQzNSwianRpIjoiMDQ1MzM2NDMtMDBiMy00MjQ4LTg3MDQtZjA2MGZmYTc5NGU4IiwiaXNzIjoiaHR0cDovL2Rldi1iYXNlLWlkZW50aXR5LXNlcnZpY2UuY29tL2F1dGgvcmVhbG1zL0Jhc2UiLCJzdWIiOiI2YTNkNjg5ZS0yNDk4LTRmMGEtOWUwZS04NjIwZjRhM2M1MmQiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZG1pbi1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiYWE0N2NkYjMtM2JkYS00ZWVjLWE2MzItY2NhMmRmYjBlZGY0IiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyJdfSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInRlc3QiOiJ2YWwxIiwibmFtZSI6IlVzZXIgT25lIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidXNlci5vbmVAbWFpbC5jb20iLCJnaXZlbl9uYW1lIjoiVXNlciIsImZhbWlseV9uYW1lIjoiT25lIiwiZW1haWwiOiJ1c2VyLm9uZUBtYWlsLmNvbSJ9.UMvKOltIG4cdlJeI-nX8rapdpvz0FybMSUZkCuoB8BOZEN4KoYAELpWlCH9DUStCUeU8y7YNk8UHehJHJOgbRw5zOEL4S9lek5ICsjb8TlUkbh7KIT0ELSp_imA3vKkeAMzmKcVM9IOR8gm76rumaDE-a22r-_2TUQ5w_AtbUq53SkcPa46C8AVDgydilm-4j8esovIadNiOPVe_Z4Op9zTzNfE0BvdW1bLMnb17aG1pitw3IjN4aTzFrYvEHM5FbV8Ph8BCKN7l34ouMND_iwuhnilMXbyOLxRe46YHZ-zuC-4jT3-DVCcU7KdOPLLLo2VsLhKxzF77rZYLifzFYg"

        response = client.get(
            "/tests/",
            headers={'Authorization': 'Bearer %s'.format(token)}
        )
        data = response.json

        assert data == \
               {'message': {'code': 'authorization_header_missing', 'description': 'Authorization header is expected'}}

    @staticmethod
    def __token__() -> str:
        algorithm = "RS256"
        exp = datetime.datetime.now() + datetime.timedelta(hours=1)
        key = RSA.generate(2048)

        header = {
            "alg": algorithm,
            "typ": "JWT",
            "kid": "hCRd_xJNbmtTvjzMOB02wgmtwz-z1x5eT3SoBhkxHU8"
        }
        payload = {
            "exp": exp,
            "iat": 1639232024,
            "jti": "819b76d4-fbbc-4ae7-8e23-93c49cfed5a3",
            "iss": "http://dev-base-identity-service.com/auth/realms/Base",
            "sub": "6a3d689e-2498-4f0a-9e0e-8620f4a3c52d",
            "typ": "Bearer",
            "azp": "admin-client",
            "session_state": "cb67f856-ea2c-42b3-ac5b-426ca152bbc9",
            "acr": "1",
            "realm_access": {
                "roles": [
                    "offline_access"
                ]
            },
            "scope": "email profile",
            "email_verified": True,
            "test": "val1",
            "name": "User One",
            "preferred_username": "user.one@mail.com",
            "given_name": "User",
            "family_name": "One",
            "email": "user.one@mail.com"
        }

        return jwt.encode(algorithm=algorithm, key=key.exportKey, payload=payload)
"""
