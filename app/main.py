from flask import Flask
from flask_jwt_oidc import JwtManager
from flask_cors import CORS
from flask_restx import Api

from app.core import utils, config

app = Flask(__name__)
app = utils.config_app(app, config.AppConfig)

auth = JwtManager(app)
CORS(app)

health = utils.config_health()
app.add_url_rule("/", "health", view_func=lambda: health.run())

API = Api(
    app,
    title="Python Service",
    description="Basic python service example. Always use the trailing path separator ('/')",
    doc="/docs"
)

from app.core import app_setup
