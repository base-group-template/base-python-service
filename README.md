# Base Python Service

Base project of a python service with the flask framework.
Code taken from the tiangolo/uwsgi-nginx-flask documentation

## To run locally
It must be deployed with: ```$ docker-compose up ```

## Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Flask Tutorial](https://flask.palletsprojects.com/en/1.1.x/tutorial/layout/)
* [Flask restx - with swagger - in use](https://flask-restx.readthedocs.io/en/latest/index.html)
* [jwt auth - in use](https://pypi.org/project/flask-jwt-oidc/)
* [Docker tiangolo/uwsgi-nginx-flask - in use](https://hub.docker.com/r/tiangolo/uwsgi-nginx-flask/)
* [Flask RESTful](https://flask-restful.readthedocs.io/en/latest/quickstart.html#a-minimal-api)
* [Flask CI/CD Tutorial](https://kubernetes.io/blog/2019/07/23/get-started-with-kubernetes-using-python/)
* [Flask JWT Auth](https://www.geeksforgeeks.org/using-jwt-for-user-authentication-in-flask/)
* [Keycloak](https://keycloak-client.readthedocs.io/en/latest/extensions/flask.html)
* [swagger docs](https://flask-restplus.readthedocs.io/en/stable/swagger.html)
* [Stanford - guide_flask_unit_testing](https://codethechange.stanford.edu/guides/guide_flask_unit_testing.html)


### envsubst and deployments

* [replace env variables](https://skofgar.ch/dev/2020/08/how-to-quickly-replace-environment-variables-in-a-file/)

### Future steps

* [OCR - pytesseract](https://pypi.org/project/pytesseract/)
* [OCR - more pytesseract](https://www.pyimagesearch.com/2020/08/03/tesseract-ocr-for-non-english-languages/)
* [OCR - pytesseract receipt example](https://hypi.io/2019/10/29/reading-text-from-invoice-images-with-python/)
* [OCR - Tables ](https://pypi.org/project/table-ocr/)
* [OCR - tutorial (in use) ](https://towardsdatascience.com/optical-character-recognition-ocr-with-less-than-12-lines-of-code-using-python-48404218cccb)

## PyCharm Absolute paths config

1. Mark app as Sources Root dir.
2. Go to: Preferences | Build, Execution, Deployment | Console | Python Console
3. Make sure that "Add source roots to PYTHONPATH" check box is active.
4. If that doesn't change recognize the absolute paths -> File | Invalidate Caches / Restart...

## Configure run configuration - local execution
It is recommended to have a virtual environment for the project to use in the "Script path".

Set the following values:
- Script path: path/to/venv/bin/flask
- Parameters: run
- Environment variables: FLASK_APP=app/main.py;FLASK_ENV=development;FLASK_RUN_PORT=7003;API_ENV=local