FROM tiangolo/uwsgi-nginx-flask:python3.8

RUN apt-get -y update && apt-get install -y python3-opencv && apt-get -y install tesseract-ocr && apt-get -y install tesseract-ocr-spa && apt-get -y install tesseract-ocr-eng && apt-get -y install tesseract-ocr-deu

COPY ./app /app
COPY ./requirements.txt /app

RUN pip install -r /app/requirements.txt